Revealator.effects_padding = '-250';

!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : jQuery)
}(function (a) {
    var b, c = navigator.userAgent, d = /iphone/i.test(c), e = /chrome/i.test(c), f = /android/i.test(c);
    a.mask = {
        definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    }, a.fn.extend({
        caret: function (a, b) {
            var c;
            if (0 !== this.length && !this.is(":hidden"))return "number" == typeof a ? (b = "number" == typeof b ? b : a, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(a, b) : this.createTextRange && (c = this.createTextRange(), c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select())
            })) : (this[0].setSelectionRange ? (a = this[0].selectionStart, b = this[0].selectionEnd) : document.selection && document.selection.createRange && (c = document.selection.createRange(), a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length), {
                begin: a,
                end: b
            })
        }, unmask: function () {
            return this.trigger("unmask")
        }, mask: function (c, g) {
            var h, i, j, k, l, m, n, o;
            if (!c && this.length > 0) {
                h = a(this[0]);
                var p = h.data(a.mask.dataName);
                return p ? p() : void 0
            }
            return g = a.extend({
                autoclear: a.mask.autoclear,
                placeholder: a.mask.placeholder,
                completed: null
            }, g), i = a.mask.definitions, j = [], k = n = c.length, l = null, a.each(c.split(""), function (a, b) {
                "?" == b ? (n--, k = a) : i[b] ? (j.push(new RegExp(i[b])), null === l && (l = j.length - 1), k > a && (m = j.length - 1)) : j.push(null)
            }), this.trigger("unmask").each(function () {
                function h() {
                    if (g.completed) {
                        for (var a = l; m >= a; a++)if (j[a] && C[a] === p(a))return;
                        g.completed.call(B)
                    }
                }

                function p(a) {
                    return g.placeholder.charAt(a < g.placeholder.length ? a : 0)
                }

                function q(a) {
                    for (; ++a < n && !j[a];);
                    return a
                }

                function r(a) {
                    for (; --a >= 0 && !j[a];);
                    return a
                }

                function s(a, b) {
                    var c, d;
                    if (!(0 > a)) {
                        for (c = a, d = q(b); n > c; c++)if (j[c]) {
                            if (!(n > d && j[c].test(C[d])))break;
                            C[c] = C[d], C[d] = p(d), d = q(d)
                        }
                        z(), B.caret(Math.max(l, a))
                    }
                }

                function t(a) {
                    var b, c, d, e;
                    for (b = a, c = p(a); n > b; b++)if (j[b]) {
                        if (d = q(b), e = C[b], C[b] = c, !(n > d && j[d].test(e)))break;
                        c = e
                    }
                }

                function u() {
                    var a = B.val(), b = B.caret();
                    if (o && o.length && o.length > a.length) {
                        for (A(!0); b.begin > 0 && !j[b.begin - 1];)b.begin--;
                        if (0 === b.begin)for (; b.begin < l && !j[b.begin];)b.begin++;
                        B.caret(b.begin, b.begin)
                    } else {
                        for (A(!0); b.begin < n && !j[b.begin];)b.begin++;
                        B.caret(b.begin, b.begin)
                    }
                    h()
                }

                function v() {
                    A(), B.val() != E && B.change()
                }

                function w(a) {
                    if (!B.prop("readonly")) {
                        var b, c, e, f = a.which || a.keyCode;
                        o = B.val(), 8 === f || 46 === f || d && 127 === f ? (b = B.caret(), c = b.begin, e = b.end, e - c === 0 && (c = 46 !== f ? r(c) : e = q(c - 1), e = 46 === f ? q(e) : e), y(c, e), s(c, e - 1), a.preventDefault()) : 13 === f ? v.call(this, a) : 27 === f && (B.val(E), B.caret(0, A()), a.preventDefault())
                    }
                }

                function x(b) {
                    if (!B.prop("readonly")) {
                        var c, d, e, g = b.which || b.keyCode, i = B.caret();
                        if (!(b.ctrlKey || b.altKey || b.metaKey || 32 > g) && g && 13 !== g) {
                            if (i.end - i.begin !== 0 && (y(i.begin, i.end), s(i.begin, i.end - 1)), c = q(i.begin - 1), n > c && (d = String.fromCharCode(g), j[c].test(d))) {
                                if (t(c), C[c] = d, z(), e = q(c), f) {
                                    var k = function () {
                                        a.proxy(a.fn.caret, B, e)()
                                    };
                                    setTimeout(k, 0)
                                } else B.caret(e);
                                i.begin <= m && h()
                            }
                            b.preventDefault()
                        }
                    }
                }

                function y(a, b) {
                    var c;
                    for (c = a; b > c && n > c; c++)j[c] && (C[c] = p(c))
                }

                function z() {
                    B.val(C.join(""))
                }

                function A(a) {
                    var b, c, d, e = B.val(), f = -1;
                    for (b = 0, d = 0; n > b; b++)if (j[b]) {
                        for (C[b] = p(b); d++ < e.length;)if (c = e.charAt(d - 1), j[b].test(c)) {
                            C[b] = c, f = b;
                            break
                        }
                        if (d > e.length) {
                            y(b + 1, n);
                            break
                        }
                    } else C[b] === e.charAt(d) && d++, k > b && (f = b);
                    return a ? z() : k > f + 1 ? g.autoclear || C.join("") === D ? (B.val() && B.val(""), y(0, n)) : z() : (z(), B.val(B.val().substring(0, f + 1))), k ? b : l
                }

                var B = a(this), C = a.map(c.split(""), function (a, b) {
                    return "?" != a ? i[a] ? p(b) : a : void 0
                }), D = C.join(""), E = B.val();
                B.data(a.mask.dataName, function () {
                    return a.map(C, function (a, b) {
                        return j[b] && a != p(b) ? a : null
                    }).join("")
                }), B.one("unmask", function () {
                    B.off(".mask").removeData(a.mask.dataName)
                }).on("focus.mask", function () {
                    if (!B.prop("readonly")) {
                        clearTimeout(b);
                        var a;
                        E = B.val(), a = A(), b = setTimeout(function () {
                            B.get(0) === document.activeElement && (z(), a == c.replace("?", "").length ? B.caret(0, a) : B.caret(a))
                        }, 10)
                    }
                }).on("blur.mask", v).on("keydown.mask", w).on("keypress.mask", x).on("input.mask paste.mask", function () {
                    B.prop("readonly") || setTimeout(function () {
                        var a = A(!0);
                        B.caret(a), h()
                    }, 0)
                }), e && f && B.off("input.mask").on("input.mask", u), A()
            })
        }
    })
});

var cars;

$(document).ready(function () {

    var type_mobile = false;

    if (device.mobile() == true || device.tablet() == true || $(window).width() < 768) {
        type_mobile = true;
    }

    if ($('.js--select-styled').length) {
        $('.js--select-styled').styler({
            onSelectOpened: function () {
                $(this).closest('.jq-selectbox').removeClass('error');
            },
            onSelectClosed: function () {
                if ($(this).hasClass('js--auto')) {
                    var index = $(this).find('option:selected').index();

                    if (index != 0) {
                        var auto = $(this).find('option:selected').val();
                        var mark = cars[auto];
                        mark = mark['model'];
                        var html = '<option selected disabled>Модель автомобиля</option>';

                        $('.js_auto').val(auto);

                        for (var ArrVal in mark) {
                            html = html + '<option value="' + ArrVal + '">' + ArrVal + '</option>';
                        }

                        $('.js--mark').styler('destroy').empty().html(html).removeAttr('disabled').styler({
                            onSelectOpened: function () {
                                $(this).closest('.jq-selectbox').removeClass('error');
                            },
                            onSelectClosed: function () {
                                if ($(this).hasClass('js--mark')) {
                                    var index = $(this).find('option:selected').index();

                                    if (index != 0) {
                                        var auto = $('.js--auto').find('option:selected').val();
                                        var model = $(this).find('option:selected').val();
                                        var year = cars[auto];
                                        year = year['model'][model];

                                        $('.js_mark').val(model);

                                        var html = '<option selected disabled>Год выпуска</option>';

                                        year.forEach(function (item) {
                                            html = html + '<option value="' + item + '">' + item + '</option>';
                                        });

                                        $('.js--year').styler('destroy').empty().html(html).removeAttr('disabled').styler({
                                            onSelectOpened: function () {
                                                $(this).closest('.jq-selectbox').removeClass('error');
                                            },
                                            onSelectClosed: function () {
                                                var year = $('.js--year').find('option:selected').val();
                                                $('.js_year').val(year);
                                            }
                                        });
                                    }
                                }
                            }
                        });

                        $('.js--year').styler('destroy').empty().attr('disabled', 'disabled').html('<option selected disabled>Год выпуска</option>').styler({
                            onSelectOpened: function () {
                                $(this).closest('.jq-selectbox').removeClass('error');
                            }
                        });
                    }
                }
            }
        })
    }

    // фокус поля
    $(document).on('focus', '.inp', function () {
        $(this).removeClass('error');

        if ($(this).closest('section').hasClass('promo')) {
            $(this).closest('form').addClass('open');
        }
    });

    // отправка формы
    $(document).on('click', '.js--form-submit', function () {

        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.inp.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');

            if (val == '' || val == undefined) {
                inp.addClass('error');
                errors = true;
            }
            else {
                if (inp.hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        $(form).find('select.required').each(function () {
            var inp = $(this);

            if (inp.find('option:nth-child(1)').is(':selected')) {
                inp.closest('.jq-selectbox').addClass('error');
            }
        });

        if (errors == false) {
            form.submit();

            //var button_value = btn.text();
            //btn.text('Подождите...');
            //
            //var method = form.attr('method');
            //var action = form.attr('action');
            //var data = form.serialize();
            //
            //$.ajax({
            //    type: method,
            //    url: action,
            //    data: data,
            //    success: function (data) {
            //        form.find('.inp').each(function () {
            //            $(this).prop('value', '')
            //        });
            //        $(form).find('.js--form-submit').text(button_value);
            //        $('.js-form-sended').trigger('click');
            //    },
            //    error: function (data) {
            //        btn.text('Ошибка');
            //        setTimeout(function () {
            //            $(form).find('.js-form-submit').text(button_value);
            //        }, 2000);
            //    }
            //});
        }

        return false;
    });

    if ($('.logos-slider').length) {
        $('.logos-slider').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                // breakpoint from 480 up
                480: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                // breakpoint from 768 up
                768: {
                    items: 2,
                    autoWidth: false,
                    dots: true
                },
                769: {
                    items: 3,
                    autoWidth: true,
                    dots: false
                }
            }
        })
    }

    if (type_mobile == true && $('.promo-list').length) {
        $('.promo-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.ransom-algorithm-list').length) {
        $('.ransom-algorithm-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.constraints-list').length) {
        $('.constraints-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.best-list').length) {
        $('.best-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.best-list').length) {
        $('.best-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.comission-condition-list').length) {
        $('.comission-condition-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.fast-list').length) {
        $('.fast-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    //if (type_mobile == true && $('.about-scheme--content-wrap').length) {
    //    $('.about-scheme--content-wrap').owlCarousel({
    //        margin: 0,
    //        loop: true,
    //        autoWidth: false,
    //        autoHeight: true,
    //        nav: true,
    //        dots: false,
    //        navText: [,],
    //        autoplay: false,
    //        items: 1
    //    })
    //}

    if (type_mobile == true && $('.about-structure-list').length) {
        $('.about-structure-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.index-vantage-list').length) {
        $('.index-vantage-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.ava-garant-list').length) {
        $('.ava-garant-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.ava-news-list').length) {
        $('.ava-news-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if (type_mobile == true && $('.contacts-photo-list').length) {
        $('.contacts-photo-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if ($('.about-promo-list').length) {
        $('.about-promo-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if ($('.about-photo-list--el').length > 1) {
        $('.about-photo-list').owlCarousel({
            margin: 0,
            loop: true,
            autoHeight: false,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: false,
            items: 1,
            responsive: {
                0: {
                    dots: true,
                    items: 1
                },
                769: {
                    dots: false,
                    items: 5
                }
            }
        })
    }

    if ($('.index-promo-list--el').length > 1) {
        $('.index-promo-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: true,
            items: 1
        })
    }

    if ($('.feedback-list--el').length > 1) {
        $('.feedback-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1
        })
    }

    if ($('.about-welcome-list--el').length > 1) {
        $('.about-welcome-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: false,
            items: 1,
            responsive: {
                0: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                480: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                768: {
                    items: 3,
                    autoWidth: false,
                    dots: true
                },
                769: {
                    items: 5,
                    autoWidth: false,
                    dots: false
                }
            }
        })
    }

    if ($('.about-blog-list--el').length > 1)  {
        $('.about-blog-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: false,
            items: 1,
            responsive: {
                0: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                480: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                768: {
                    items: 2,
                    autoWidth: false,
                    dots: true
                },
                1080: {
                    items: 3,
                    autoWidth: false,
                    dots: false
                }
            }
        })
    }

    if ($('.index-special-list--el').length > 1) {
        $('.index-special-list').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: true,
            navText: [,],
            autoplay: false,
            items: 1,
            responsive: {
                0: {
                    items: 1,
                    autoWidth: false,
                    dots: true
                },
                769: {
                    items: 3,
                    autoWidth: false,
                    dots: false
                }
            },
            onInitialized: function () {
                $('.index-special .owl-item.active').eq(1).addClass('active-el');
            },
            onTranslate: function () {
                $('.index-special .owl-item').removeClass('active-el');
            },
            onTranslated: function () {
                $('.index-special .owl-item.active').eq(1).addClass('active-el');
            }
        })
    }

    $('.acc_container').hide();

    $('.acc_trigger').click(function () {
        if ($(this).next().is(':hidden')) {
            $('.acc_trigger').removeClass('active').next().slideUp();
            $(this).toggleClass('active').next().slideDown();
        }
        else {
            $(this).toggleClass('active').next().slideUp();
        }
        return false;
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp').focus(function () {
        $(this).removeClass('error')
    });

    $(".inp-phone").mask("+9 (999) 999-99-99");

    // отправка формы
    $(document).on('click', '.js--form-submit', function () {

        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '') {
                inp.addClass('error');
                errors = true;
            }
            else {
                if (inp.hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            form.submit();
            //var button_value = btn.text();
            //btn.text('Подождите...');
            //
            //var method = form.attr('method');
            //var data = form.serialize();
            //var action = form.attr('action');
            //
            //$.ajax({
            //    type: method,
            //    url: action,
            //    data: data,
            //    success: function (data) {
            //        form.find('.inp').each(function () {
            //            $(this).prop('value', '')
            //        });
            //
            //        form.find('.jq-selectbox').each(function(){
            //            var option = $('select option:first-child');
            //            $(this).find('select option').removeAttr('selected');
            //            option.attr('selected', 'selected');
            //            $(this).find('.jq-selectbox__dropdown ul li').removeClass('selected');
            //            $(this).find('.jq-selectbox__dropdown ul li:first-child').addClass('selected');
            //            $(this).find('.jq-selectbox__select-text').text(option.val());
            //        });
            //
            //        btn.text(button_value);
            //        var thanks_link = form.data('thanks');
            //
            //        $.fancybox.open({
            //            src  : thanks_link,
            //            type : 'ajax'
            //        });
            //        //$('.js--form-ok').trigger('click');
            //    },
            //    error: function (data) {
            //        btn.text('Ошибка');
            //        setTimeout(function () {
            //            btn.text(button_value);
            //        }, 2000);
            //    }
            //});
        }

        return false;
    });

    function e() {
        //function t() {
        //    var t, o, i = [55.858022, 37.395852];
        //    t && g.geoObjects.remove(t), e.get({provider: "browser", mapStateAutoApply: !0}).then(function (e) {
        //        e.geoObjects.options.set({"preset": "islands#yellowDotIcon"}), g.geoObjects.add(e.geoObjects), o = e.geoObjects.position, ymaps.route([o, i] , {mapStateAutoApply: !0}).then(function (e) {
        //            e.getPaths().options.set({strokeColor: "fbd34bff"}), g.geoObjects.add(t = e)
        //        })
        //    })
        //}

        var e = ymaps.geolocation;
        g = new ymaps.Map("map", {
            center: [55.858022, 37.395852],
            zoom: 14,
            controls: ["zoomControl"]
        }), g.controls.add("zoomControl", {
            left: 5,
            top: 5
        });


	var mplcmrk = new ymaps.Placemark([55.858022, 37.395852], { hintContent: 'АВТОВЭЙ', balloonContent: '<b>АВТОВЭЙ - автомобильный центр</b><br/>71 км МКАД Путилково, 16 А<br/>+7 (495) 797-55-88<br/>info@wayauto.ru' },{iconColor: '#ff930c'});
	g.geoObjects.add(mplcmrk);
        g.behaviors.disable("scrollZoom");
        /*, g.behaviors.disable("scrollZoom"), g.behaviors.disable("drag"), g.geoObjects.add(new ymaps.Placemark([55.705968, 37.647239], {}, {
         iconLayout: "default#image",
         }));*/
        // return t(), !1;
        //t();
    }

    if ($("#map").length) {
        var g;
        ymaps.ready(e)
    }

    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');
        return false;
    });


    $(document).on("scroll", function () {
        if ($(document).scrollTop() > 50) {
            $('.header').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
        }
    });

    $('.js--show-more').on('click', function () {
        $(this).closest('section').find('.hidden-block').removeClass('hidden-block');
        $(this).addClass('hidden-block');
        return false;
    });

    $('ul.tabs--caption').each(function () {
        $(this).find('li').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('.about-scheme--content-wrap').each(function () {
                        $(this).find('.tabs--content').removeClass('active').eq(i).addClass('active');
                    })
            });
        });
    });

    //if ($('.zoom-photo').length && type_mobile == false) {
    //    $(".zoom-photo").each(function () {
    //        $(this).imagezoomsl({
    //            zoomrange: [1, 12],
    //            zoomstart: 4,
    //            innerzoom: true,
    //            magnifierborder: "none"
    //        });
    //    })
    //}

    $('.tabs--location li a').hover(function () {
        var thos = $(this);
        var index_tab = thos.closest('.tabs--content').index();
        var index_el = thos.closest('li').data('popup');

        $('.about-scheme--img-wrap .tabs--content').eq(index_tab).find('.pointer .pointer--el').eq(index_el - 1).find('.content').fadeIn(200);
    }, function () {
        $('.pointer--el .content').fadeOut(200);
    });

    $('.tabs--location li a').on('click', function () {
        return false;
    })

    if(device.iphone() == 'iPhone'){
        $(document).on('click', '.jq-selectbox__select', function(){
            alert('11');
        })
    }
});